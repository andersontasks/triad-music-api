<?php 

namespace App\Repositories;

use App\Models\Historico;
use App\Repositories\UsuarioRepository;

class HistoricoRepository 
{
    public static function historico($request) 
    {
        $usuario = UsuarioRepository::getUsuario($request);

        return Historico::with('artista')
        ->where('usuario', $usuario->id)
        ->orderBy('id', 'desc')
        ->get();
    }    

    public static function save($request) {        
        $usuario = UsuarioRepository::getUsuario($request);

        $historico = new Historico;
        $historico->usuario = $usuario->id;
        $historico->id_artista = $request->id;
        $historico->save();        
    }

}